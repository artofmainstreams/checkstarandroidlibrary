package com.google.sample.cloudvision;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by frien on 20.06.2017.
 */

public class Products {
    private String shopName;
    private String date;
    private String time;
    private List<Product> products = new LinkedList<>();

    @Override
    public String toString() {
        StringBuilder message = new StringBuilder();
        message.append("Название магазина ");
        message.append(shopName);
        message.append("\nДата покупки ");
        message.append(date);
        message.append("\nВремя покупки: ");
        message.append(time);
        for (Product product : products) {
            message.append("\nНазвание: ");
            message.append(product.getProduct());
            message.append("\nЦена: ");
            message.append(product.getPrice());
        }
        return message.toString();
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(String product, String price) {
        products.add(new Product(product, price));
    }

    private class Product {
        private String product;
        private String price;

        Product(String product, String price) {
            this.product = product;
            this.price = price;
        }

        String getProduct() {
            return product;
        }

        String getPrice() {
            return price;
        }
    }
}

package com.google.sample.cloudvision;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.google.sample.cloudvision.MainActivity.MYSERVICE;

public class NotificationListener extends NotificationListenerService {
    BroadcastReceiver br;

    private String TAG = "myLog";
    @Override
    public void onCreate() {
        super.onCreate();
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getStringExtra("command").equals("list")){
                    Log.e("myLog", "command list");
                    int i = 1;
                    if (NotificationListener.this.getActiveNotifications() == null) {
                        Log.e("myLog", "Null!");
                    } else {
                        for (StatusBarNotification sbn : NotificationListener.this.getActiveNotifications()) {
                            Date date = new Date(sbn.getNotification().when);
                            DateFormat formatter = new SimpleDateFormat("HH:mm");
                            String dateFormatted = formatter.format(date);

                            Log.d(TAG, i +" - name: " + sbn.getPackageName()
                                    + "\ncategory: " + sbn.getNotification().category
                                    + "\ncontent: " + sbn.getNotification().tickerText
                                    + "\ntime: " + dateFormatted);
                            i++;
                        }
                    }
                }
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(MYSERVICE);
        registerReceiver(br,filter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(br);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        Date date = new Date(sbn.getNotification().when);
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        String dateFormatted = formatter.format(date);

        Log.d(TAG, "new - name: " + sbn.getPackageName()
                + "\ncategory: " + sbn.getNotification().category
                + "\ncontent: " + sbn.getNotification().tickerText
                + "\ntime: " + dateFormatted);
    }
}